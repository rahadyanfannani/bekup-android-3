package com.rahadyan.bekup_android_3;

import com.rahadyan.bekup_android_3.models.City;
import com.rahadyan.bekup_android_3.models.Movie;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by rahad on 21/10/2016.
 */

public interface BekupService {
    String BASE_URL = "http://ibacor.com/api/";
    @GET("jadwal-bioskop")
    Call<City> getCity();

    @GET("jadwal-bioskop")
    Call<Movie> getMovieByCity(@Query("id") String id);

    class Factory {
        private static BekupService service;

        public static BekupService getInstance(){
            if(service == null){
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                service = retrofit.create(BekupService.class);
                return service;
            } else {
                return service;
            }
        }
    }
}

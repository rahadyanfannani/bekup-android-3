package com.rahadyan.bekup_android_3.models;

import java.util.List;

/**
 * Created by rahad on 21/10/2016.
 */

public class Movie {
    private String status;
    private String kota;
    private String date;
    private List<Data> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public static class Data {
        private String movie;
        private String poster;
        private String genre;
        private String duration;

        public String getMovie() {
            return movie;
        }

        public void setMovie(String movie) {
            this.movie = movie;
        }

        public String getPoster() {
            return poster;
        }

        public void setPoster(String poster) {
            this.poster = poster;
        }

        public String getGenre() {
            return genre;
        }

        public void setGenre(String genre) {
            this.genre = genre;
        }

        public String getDuration() {
            return duration;
        }

        public void setDuration(String duration) {
            this.duration = duration;
        }

        public List<Jadwal> getJadwal() {
            return jadwal;
        }

        public void setJadwal(List<Jadwal> jadwal) {
            this.jadwal = jadwal;
        }

        private List<Jadwal> jadwal;
        public static class Jadwal{
            private String bioskop;
            private List<String> jam;
            private String harga;

            public String getBioskop() {
                return bioskop;
            }

            public void setBioskop(String bioskop) {
                this.bioskop = bioskop;
            }

            public List<String> getJam() {
                return jam;
            }

            public void setJam(List<String> jam) {
                this.jam = jam;
            }

            public String getHarga() {
                return harga;
            }

            public void setHarga(String harga) {
                this.harga = harga;
            }
        }
    }

}

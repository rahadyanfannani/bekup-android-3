package com.rahadyan.bekup_android_3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.rahadyan.bekup_android_3.adapters.CityAdapter;
import com.rahadyan.bekup_android_3.models.City;
import com.rahadyan.bekup_android_3.models.Movie;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {

    private BekupService service;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private String mDataSet[];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = (RecyclerView) findViewById(R.id.kota_recycle_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);



        Retrofit.Builder builder = new Retrofit.Builder();
         service = BekupService.Factory.getInstance();
        getDataApi();
    }

    protected void getDataApi(){
        service.getCity().enqueue(new Callback<City>() {
            @Override
            public void onResponse(Call<City> call, Response<City> response) {
                City kota = response.body();


                for(City.Data data : kota.getData()){
                    Log.d("kota : ",data.getKota());
                }
                mAdapter = new CityAdapter(kota);
                mRecyclerView.setAdapter(mAdapter);

            }

            @Override
            public void onFailure(Call<City> call, Throwable t) {
                Log.d("kota : ","error"+t.getMessage().toString());

            }
        });
        service.getMovieByCity("18").enqueue(new Callback<Movie>() {
            @Override
            public void onResponse(Call<Movie> call, Response<Movie> response) {
               Movie data =  response.body();
                for(Movie.Data movie : data.getData()){
                    Log.d("Movie : ",movie.getMovie());
                }
            }

            @Override
            public void onFailure(Call<Movie> call, Throwable t) {

            }
        });
        // specify an adapter (see also next example)


    }

}
